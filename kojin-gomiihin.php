<?php $id="kojin_gomiihin"; ?>
<?php require_once($_SERVER['DOCUMENT_ROOT']."/include/init.php"); ?>
<?=$head1; ?>
<?=$header; ?>

    <div class="c-path">
        <ul>
            <li><a href="./">ホーム </a></li>
            <li><a href="#">個人のお客様  </a></li>
            <li><a href="#">不用品回収・ゴミ処分・遺品整理</a></li>
        </ul>
    </div>
    <div class="gomiihin">
        <img src="images/banner_kojin_gomiihin.png" width="1002" height="302" alt="banner_kojin_gomiihin" class="banner_gomiihin">
        <div class="c-kojinTitle">
            <h2>不用品回収・ゴミ処分・遺品整理 </h2>
        </div>
        <div class="l-main">
            <div class="l-conts">
                <div class="c-gomiihin">
                    <p class="pagh01">
                        リサイクルマスター英雄では、東京都を拠点として10年あまり、東京都/千葉県/埼玉県/茨城県/神奈川県など関東圏で不用品回収・ゴミ処分・遺品整理に関するお仕事をさせていただいております。不用品の回収、状態の良いものは買い取りもさせていただいております。大量の処分品でも当社で分別作業して廃棄致しますので個人の方はもちろん、法人の方、お時間がない方もお気軽にお申込みください。また、リサイクルマスター英雄は不用品処分、ゴミ回収の業界最安値を目指しております。 まずはフリーダイヤルで無料お見積もりをさせてください。 他社との比較歓迎です！総額で比べてください。万一他社との見積もりと比べて1円でも高かった場合には再度お見積もりさせてください。お客様が納得できる金額を再度ご提示させて頂きます！
                    </p>
                    <div class="c-titleMain">
                        <h2>回収品目例</h2>
                    </div>
                    <ul>
                        <li>
                            <img src="images/gomiihin_01.png" width="160" height="160" alt="gomiihin_photo_01">
                        </li>
                        <li>
                            <img src="images/gomiihin_02.png" width="160" height="160" alt="gomiihin_photo_02">
                        </li>
                        <li>
                            <img src="images/gomiihin_03.png" width="160" height="160" alt="gomiihin_photo_03">
                        </li>
                        <li>
                            <img src="images/gomiihin_04.png" width="160" height="160" alt="gomiihin_photo_04">
                        </li>
                        <li>
                            <img src="images/gomiihin_05.png" width="160" height="160" alt="gomiihin_photo_05">
                        </li>
                        <li>
                            <img src="images/gomiihin_06.png" width="160" height="160" alt="gomiihin_photo_06">
                        </li>
                        <li>
                            <img src="images/gomiihin_07.png" width="160" height="160" alt="gomiihin_photo_07">
                        </li>
                        <li>
                            <img src="images/gomiihin_08.png" width="160" height="160" alt="gomiihin_photo_08">
                        </li>
                    </ul>
                    <p class="pagh02">
                        回収品目に関しましてはごく一部の例です。品目に関しましてはお気軽にご相談下さい。<br>
                        また、リサイクルマスター英雄ではゴミ処分や遺品整理も行っております。処分等でお困りでしたらお気軽にご相談下さい
                    </p>
                    <p class="pagh03"> 
                        処分費用に関しましては価格表をご確認下さい。<br>
                        <a href="#">不用品回収専用のサイトも</a> 運営しております。
                    </p>
                </div>
                <?=$map; ?>
            </div>
            <?php require_once($_SERVER['DOCUMENT_ROOT']."/include/side.php"); ?>
        </div>
    </div>

<?=$footer; ?>