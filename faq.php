<?php $id="FAQ"; ?>
<?php require_once($_SERVER['DOCUMENT_ROOT']."/include/init.php"); ?>
<?=$head1; ?>
<?=$header; ?>

    <div class="c-path">
        <ul>
            <li><a href="./">ホーム </a></li>
            <li><a href="#">よくある質問</a></li>
        </ul>
    </div>
    <div class="faq">
        <div class="l-main">
            <div class="l-conts">
                <div class="c-faq">
                    <div class="c-titleMain">
                        <h2>よくある質問</h2>
                    </div>
                    <div class="c-faq__body">
                        <div class="c-faq__body__title">
                            <h2>Q.　不用品の買取・回収は自宅・オフィスまで取りに来てくれますか。</h2>
                        </div>
                        <div class="c-faq__body__content">
                            はい、自宅・オフィスまで出張買取・回収にお伺い致します。出張買取では、トラック代・燃料費など経費の関係上、出張距離・作業内容等を考慮した上で、出張買取・回収にお伺いできないことがございますが、無料回収または有料引き取りでなら出張可能な場合もございます。この点、御理解いただければと思います。<br>
                            <br>
                            ＜出張買取・回収ができない事例＞<br>
                            「自宅の自転車を一台のみを回収してほしい」等

                        </div>
                        <div class="c-faq__body__title">
                            <h2>Q.　厨房機器・オフィス機器・自宅の不用品を丸ごと回収・買取してほしい。</h2>
                        </div>
                        <div class="c-faq__body__content">
                            はい、リサイクルマスター英雄では、丸ごと回収・買取を御希望の方向けに、「トラック詰め放題パック」というものをご用意しております！「引っ越しに伴い不要なものは全部処分したい」「オフィス用品をまるごと買取・回収してほしい」「厨房機器を格安で処分したい」等の御要望がござましたらお気軽にご相談下さい。安さには自信があります！
                        </div>
                        <div class="c-faq__body__title">
                            <h2>Q.　買取・回収日時の指定はできますか</h2>
                        </div>
                        <div class="c-faq__body__content">
                            はい、指定いただくことは可能です。土日祝日は大変混み合いますため、できる限りあらかじめご予約・ご相談をいただければと思います。
                        </div>
                        <div class="c-faq__body__title">
                            <h2>Q.　当日、買取・回収依頼品目が増えても大丈夫ですか。　</h2>
                        </div>
                        <div class="c-faq__body__content">
                            はい、大丈夫です。当日担当させていただくスタッフにお気軽にご相談ください。追加品目を含めた料金を再度ご提示させていただきます。
                        </div>
                        <div class="c-faq__body__title">
                            <h2>Q.　引っ越し時の不用品回収依頼は何日前に連絡すればいいですか。</h2>
                        </div>
                        <div class="c-faq__body__content">
                            約2～3週間ぐらい前のご相談が最適ですが、当日対応ができる場合もございます。
                        </div>
                        <div class="c-faq__body__title">
                            <h2>Q.　見積りに費用はかかりますか。</h2>
                        </div>
                        <div class="c-faq__body__content">
                            お見積りは無料で承ります！お気軽にご連絡ください。
                        </div>
                        <div class="c-faq__body__title">
                            <h2>Q.　とりあえず相談をしたいのですが・・・。</h2>
                        </div>
                        <div class="c-faq__body__content">
                            お見積り・ご相談は無料です！本ページに記載されていないことでも、不明な点等ございましたら、お気軽にご相談ください。２４時間電話対応を受け付けております！
                        </div>
                    </div>
                </div>
            </div>
            <?php require_once($_SERVER['DOCUMENT_ROOT']."/include/side.php"); ?>
        </div>
    </div>

<?=$footer;  ?>