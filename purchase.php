<?php $id="flow_of_purchase"; ?>
<?php require_once($_SERVER['DOCUMENT_ROOT']."/include/init.php"); ?>
<?=$head1; ?>
<?=$header; ?>

    <div class="c-path">
        <ul>
            <li><a href="./">ホーム </a></li>
            <li><a href="#">買取・回収の流れ</a></li>
        </ul>
    </div>
    <div class="purchase">
        <div class="l-main">
            <div class="l-conts">
               <?=$flow_purchase;  ?>
                <div class="c-shopItems clearfix">
                    <?=$shopping_item; ?>
                </div>
                <div class="c-map">
                    <?=$map; ?>
                </div>
            </div>
            <?php require_once($_SERVER['DOCUMENT_ROOT']."/include/side.php"); ?>
        </div>
    </div>
<?=$footer;  ?>