<?php $id="price_list"; ?>
<?php require_once($_SERVER['DOCUMENT_ROOT']."/include/init.php"); ?>
<?=$head1; ?>
<?=$header; ?>

    <div class="c-path">
        <ul>
            <li><a href="./">ホーム </a></li>
            <li><a href="#">価格表</a></li>
        </ul>
    </div>
    <div class="priceList">
        <div class="l-main">
            <div class="l-conts">
                <div class="c-priceList">
                     <div class="c-titleMain">
                        <h2>格安お得なトラック積み詰め放題</h2>
                    </div>
                    <div class="c-priceList__body">
                        <div class="c-priceList__Box">
                            <div class="c-priceList__Box__title">
                                <h2>押し入れ〜1K程度の小さなお部屋の片付けにおすすめ</h2>
                            </div>
                            <div class="c-priceList__Box__content">
                                <div class="photo_priceList">
                                    <img src="images/photo_pricelist01.png" alt="photo_price_list" width="250" height="222" >
                                </div>
                                <div class="info_priceList">
                                    <h2>軽（平）トラック1台積み詰め放題</h2>
                                    <span>￥19,000-</span>
                                    <p class="p1"> 押し入れ〜1K程度の小さなお部屋を片付けるのに、<br>
                                        おすすめのパックです。</p>
                                    <ul>
                                        <li><a href="">1Fの作業員1名（助手1名5,000円〜）</a></li>
                                        <li><a href="">2F〜最大25,000円</a></li>
                                    </ul>
                                    <p class="p2">※家電・鋳物のみなら<span>￥16,000-</span></p>
                                    <p class="p3">※不用品回収作業60分程度を基本料金の最低目安にしております。</p>
                                </div>
                                <div class="ex clearfix">
                                    回収一例 <br>
                                    シングルベッド（脚付）/カラーボックス（3段）/センターテーブル/物干し竿 /石油ファンヒーター /衣装ケース(3段)/掃除機/コンポ/スーツケース/段ボール箱×1/電子レンジ/電気ポット/ギター/2ドア冷蔵庫/全自動洗濯機/薄型テレビ
                                </div>
                            </div>
                        </div>
                        <div class="c-priceList__Box">
                            <div class="c-priceList__Box__title">
                                <h2>1K〜1DK程度のお部屋の片付けにおすすめ</h2>
                            </div>
                            <div class="c-priceList__Box__content">
                                <div class="photo_priceList">
                                    <img src="images/photo_pricelist02.png" alt="photo_price_list_02" width="250" height="222" >
                                </div>
                                <div class="info_priceList">
                                    <h2>1トン平トラック1台積み詰め放題</h2>
                                    <span>￥29,000-</span>
                                    <p class="p1"> 1K～１DK程度のお部屋を片付けるのに、おすすめのパックです。</p>
                                    <ul>
                                        <li><a href="">1Fの作業員1名（助手1名5,000円〜）</a></li>
                                        <li><a href="">2F〜最大35,000円</a></li>
                                    </ul>
                                    <p class="p3">※不用品回収作業60分程度を基本料金の最低目安にしております。</p>
                                </div>
                                <div class="ex clearfix">
                                    回収一例 <br>
                                    シングルベッド（脚付）/カラーボックス（3段）/テレビボード/オーブンレンジ/電気ポット/掃除機/木製デスク/キャビネット/整理箪笥/スーツケース/2ドア冷蔵庫/全自動洗濯機/薄型テレビ/エアコン/ゴミ袋(45L)×2/その他…
                                </div>
                            </div>
                        </div>
                        <div class="c-priceList__Box">
                            <div class="c-priceList__Box__title">
                                <h2>1DK〜2DK程度のお部屋の片付けにおすすめ</h2>
                            </div>
                            <div class="c-priceList__Box__content">
                                <div class="photo_priceList">
                                    <img src="images/photo_pricelist03.png" alt="photo_price_list_03" width="250" height="222" >
                                </div>
                                <div class="info_priceList">
                                    <h2>2トン平トラック1台積み詰め放題</h2>
                                    <span>￥40,000-</span>
                                    <p class="p1"> 1DK～2DK程度のお部屋を片付けるのに、おすすめのパックです。</p>
                                    <ul>
                                        <li><a href="">1Fの作業員1名（助手1名5,000円〜）</a></li>
                                        <li><a href="">2F〜最大50,000円</a></li>
                                    </ul>
                                    <p class="p3">※不用品回収作業90分程度を基本料金の最低目安にしております。</p>
                                </div>
                                <div class="ex clearfix">
                                    回収一例 <br>
                                    シングルベッド（脚付）/扇風機 /カラーボックス（3段）/スーツケース /テレビボード/スノーボード/物干し竿/整理箪笥/石油ファンヒーター/木製デスク/掃除機/キャビネット/コンポ/布団（1組）/デスクトップパソコン/ダンボール箱×2/インクジェットプリンター/ごみ袋（45ℓ）×2/オーブンレンジ/2ドア冷蔵庫(109ℓ)/全自動洗濯機(4.2kg)/薄型テレビ/エアコン/その他…
                                </div>
                            </div>
                        </div>
                        <div class="c-priceList__Box">
                            <div class="c-priceList__Box__title">
                                <h2>2DK〜2LDK程度のお部屋の片付けにおすすめ</h2>
                            </div>
                            <div class="c-priceList__Box__content">
                                <div class="photo_priceList">
                                    <img src="images/photo_pricelist04.png" alt="photo_price_list_04" width="250" height="245" >
                                </div>
                                <div class="info_priceList">
                                    <h2>2トン箱トラック1台積み詰め放題</h2>
                                    <span>￥80,000-</span>
                                    <p class="p1"> 2DK～2LDK程度のお部屋を片付けるのにおすすめのパックです。冷蔵庫、洗濯機、テレビ、エアコン、パソコンも一緒に回収できます。</p>
                                    <ul>
                                        <li><a href="">1Fの作業員2名（助手1名5,000円〜）</a></li>
                                        <li><a href="">2F〜最大95,000円</a></li>
                                    </ul>
                                    <p class="p3">※不用品回収作業120分程度を基本料金の最低目安にしております。。</p>
                                </div>
                                <div class="ex clearfix">
                                    回収一例 <br>
                                    ダブルベッド/DVDプレーヤー/カラーボックス（3段）/コンポ/サイドボード/エアコン/物干し竿/扇風機/石油ファンヒーター/空気清浄機/掃除機/食器棚 /シーリングライト/二人掛けソファー/デスクトップパソコン/センターテーブル/インクジェットプリンター/布団（1組）/オーブンレンジ/ダンボール箱×4/電気ポット/ごみ袋（45ℓ）×3/ガステーブル（2口）/6ドア冷蔵庫(450ℓ)/全自動洗濯機(7.0kg)/薄型テレビ/その他…
                                </div>
                            </div>
                        </div>
                        <div class="c-priceList__Box">
                            <div class="c-priceList__Box__title">
                                <h2>ハーフプラン。お得なプランもございます。</h2>
                            </div>
                            <div class="c-priceList__Box__content">
                                <div class="photo_priceList">
                                    <img src="images/photo_pricelist05.png" alt="photo_price_list_05" width="250" height="200" >
                                </div>
                                <div class="info_priceList">
                                    <h2>軽トラ1台 鋳物・家電のみなら</h2>
                                    <span>￥16,000-</span>
                                    <h2>軽トラ1台 半分の不用品回収</h2>
                                    <span>￥9,000-</span>
                                    <p class="p1"> そんなに多くの容量はいらない‥という方向けに、お得なハーフプランもございます。2トントラックでもお安くできますので、お気軽にご相談下さい。</p>
                                    <p class="p3">※軽トラ1台半分の不用品回収9,000円の料金は東京・千葉・埼玉の一部地域限定のプランとなっております。</p>
                                </div>
                            </div>
                        </div>
                        <section class="c-notice">
                            <div class="c-notice__title">
                                <h2>ご利用時の注意</h2>
                            </div>
                            <div class="c-notice__body">
                                <ul>
                                    <li><a href="#">申込み時に搬出経路、階数、品物をお聞きしますが、状況によりオプション料金が掛かる事が有ります。</a></li>
                                    <li><a href="#">危険物などは事前にお客様で分別していただく必要が有ります（ライター、マッチ、スプレー缶など）</a></li>
                                    <li><a href="#">危険物、医療廃棄物、生ゴミ、動物の死骸、血液などが付着したものはお引き受けできません。</a></li>
                                    <li><a href="#">金庫やピアノなどの特殊重量物、建築系廃棄物、消火器、可燃物、液体、食品、土砂石などは定額パックの対象外 (別途料金)となります。</a></li>
                                    <li><a href="#">回収の都合上、記載のトラックより大型の車両での回収になる場合が有ります。</a></li>
                                    <li><a href="#">テレビ、冷蔵庫、ソファー、ソファーベッドは各１台までの回収が基本パックとなっております。２台目以降の回収は 別途料金が発生する場合がございますので、予めご相談ください。</a></li>
                                </ul>
                            </div>
                        </section>
                    </div>
                </div>
                <div class="c-calFee">
                    <div class="c-titleMain">
                        <h2>料金の計算方法</h2>
                    </div>
                    <div class="c-calFee__body">
                        <p>
                            リサイクルマスター英雄は明朗会計です！<br>
                            基本料金2000円に下記個別回収料金を足してください。<br>
                            個別回収料金が8000円を越す場合は基本料金は無料とさせて頂きます！<br>
                            多数の回収品が有る場合はトラック積み放題プランの方がお得な場合がありますのでご比較ください。
                        </p>
                        <section class="c-exCalculation">
                            <div class="c-exCalculation__title">
                                <h2>料金の計算方法の一例</h2>
                            </div>
                            <div class="c-exCalculation__content">
                                <img src="images/ex_photo01.png" width="" height="" alt="">
                                <img src="images/arrow_cal.png" width="36" height="40" alt="" class="arrowCal">
                                <img src="images/ex_photo02.png" width="" height="" alt="">
                                <img src="images/arrow_cal.png" width="36" height="40" alt="" class="arrowCal2">
                                <img src="images/ex_photo03.png" width="" height="" alt="">
                                <img src="images/arrow_cal.png" width="36" height="40" alt="" class="arrowCal3">
                                <div class="amount">
                                    <h3>精算額　<span>8,500円</span></h3>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
                <div class="infoPrice clearfix">
                    <h3><span>※</span> 特殊作業料金とは・・・</h3>
                    <p> 特殊作業料金は階段を通る運び出し（大きな家具や家電を運び出す際）に掛かる料金例です。<br>
                        50キロ以上の回収物の場合は別途見積りとなります。<br>
                        大きい回収物で室内からは運び出せない場合にベランダや窓から吊り下ろしになる場合や、エアコン回収で取り外し作業が発生する場合、18：00以降の夜間作業の場合は、別途見積りとなります。予めご相談ください。
                    </p>
                </div>
            </div>
             <?php require_once($_SERVER['DOCUMENT_ROOT']."/include/side.php"); ?>
        <div>
    </div>
<?=$footer;  ?>