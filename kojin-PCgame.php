<?php $id="kojin_PCgame"; ?>
<?php require_once($_SERVER['DOCUMENT_ROOT']."/include/init.php"); ?>
<?=$head1; ?>
<?=$header; ?>

    <div class="c-path">
        <ul>
            <li><a href="./">ホーム </a></li>
            <li><a href="#">個人のお客様  </a></li>
            <li><a href="#">パソコン・ゲーム機・小型家電 無料回収</a></li>
        </ul>
    </div>
    <div class="tenpo">
        <img src="images/banner_kojin_pcgame.png" width="1002" height="302" alt="banner_kojin_pcgame">
        <div class="c-kojinTitle">
            <h2>パソコン・ゲーム機・小型家電 無料回収</h2>
        </div>
    </div>
    <div class="l-main">
        <div class="l-conts">
            <div class="c-kagukaden">
               <img src="images/kojin_photo04.png" width="700" height="505" alt="">
            </div>
            <div class="c-flow">
                <div class="c-titleMain">
                    <h2>手順</h2>
                </div>
                <div class="c-flow__body">
                    <img src="images/kojin_procedure.png" width="645" height="135" alt="kojin_precedure">
                    <section class="c-flow__body__feature01">
                        <div class="feature01-content">
                            <span class="redS">処分可能かどうかのチェック</span>
                            <p>
                                ＜ＰＣ関係＞<br>
                                ○処分可能例<br>
                                タブレット・スマホ・デスクトップ・ノートPC・液晶モニタ・MAC・オフィス・アダプタ・ケーブル・マウス・キーボード・プレイステーション・ニンテンドー・XBOX等 <br>
                            </p>
                            <p>
                                ○処分不可例 <br>
                                ブラウン管モニタ・ブラウン管パソコン・テレビ・ビデオ・画面に損傷があるパソコン・パーツのみ・プリンタ・ワープロ・スキャナ等 <br>
                            </p>
                            <p>
                                その他処分ご希望の品がございましたら下記番号までお気軽にご相談ください。
                            </p>
                            <div class="contact">
                                <h4>お気軽にお問い合わせ下さい</h4>
                                <p>
                                    <img src="images/contact_feature.png" width="47" height="25" alt="contact_feature">
                                    <span class="tell">0120-485-888</span> 携帯・PHSからもOK!
                                </p>
                                <p>お電話がつながりにくい場合  070-2177-7772 <span class="redS2">24時間対応</span></p>
                            </div>
                        </div>
                        <div class="feature01-image">
                            <img src="images/kojin_feature01.png" width="220" height="180" alt="image_feature_01">
                            <br>
                            <img src="images/kojin_feature02.png" width="220" height="180" alt="image_feature_02">
                        </div>
                    </section>
                    <img src="images/arrow_feature.png" width="36" height="45" alt="" class="arrowK">
                    <section class="c-flow__body__feature02">
                        <div class="feature02-content">
                            <span class="redS">包装</span>
                            <p> 製品を段ボールに包装します。<br>
                                ＊段ボール代は弊社では負担致しません。<br>
                                ＊付属品等はパソコン本体と同じ箱に入れてください。</p>
                        </div>
                        <div class="feature02-image">
                            <img src="images/kojin_feature03.png" width="220" height="160" alt="image_feature_03">
                        </div>
                    </section>
                        <img src="images/arrow_feature.png" width="36" height="45" alt="" class="arrowK1">
                    <section class="c-flow__body__feature03">
                        <div class="feature03-content">
                            <span class="redS">配送</span>
                            <p> 製品を配送します。<br>
                                ＊配送連絡は不要です。<br>
                                ＊必ずクロネコヤマト「宅急便」をご利用下さい。<br>
                                ＊送料は弊社では負担致しません。
                            </p>
                        </div>
                        <div class="feature03-image">
                            <img src="images/kojin_feature04.png" width="222" height="160" alt="image_feature_04">
                        </div>
                    </section>
                    <section class="c-flow__body__feature04">
                        <div class="feature04-title">
                            <h2>クロネコヤマト宅急便（お届け先）</h2>
                        </div>
                        <div class="feature04-content">
                            <ul>
                                <li><a href="#">郵便番号： 270-2232</a></li>
                                <li><a href="#">住所：千葉県松戸市和名ヶ谷1333</a></li>
                                <li><a href="#">電話番号：0120-485-888</a></li>
                                <li><a href="#">氏名：リサイクルマスター英雄</a></li>
                            </ul>
                        </div>
                    </section>
                </div>
            </div>
        </div>
        <?php require_once($_SERVER['DOCUMENT_ROOT']."/include/side.php"); ?>
    </div>
<?=$footer; ?>