<?php $id="kojin_kagukaden"; ?>
<?php require_once($_SERVER['DOCUMENT_ROOT']."/include/init.php"); ?>
<?=$head1; ?>
<?=$header; ?>

    <div class="c-path">
        <ul>
            <li><a href="./">ホーム </a></li>
            <li><a href="#">個人のお客様  </a></li>
            <li><a href="#">家具家電買取・無料回収</a></li>
        </ul>
    </div>
    <div class="tenpo">
        <img src="images/banner_kojin_kagukaden.png" width="1002" height="302" alt="banner_kojin_kagukaden">
        <div class="c-kojinTitle">
            <h2>買取・回収品目例</h2>
        </div>
    </div>
    <div class="l-main">
        <div class="l-conts">
            <p>お引越しやお部屋の模様替え等でいらなくなった家庭から出る不用品を丸ごと買い取り、無料回収、格安処分を承ります。処分をご検討の際はお気軽にご相談ください！相談・お見積りは無料！即日・夜間でも対応が可能です！</p>
            <div class="c-kagukaden">
                <div class="c-titleMain">
                    <h2>買取・回収品目例</h2>
                </div>
                <p>
                    冷蔵庫・洗濯機・テーブル・テレビ・掃除機・電子レンジ・パソコン・ソファ・棚・タンス・生活用品など、その他にもいろいろお取り扱いしております。 <br>
                    ＊回収用品に関しましてはお気軽にご相談下さい。
                </p>
                <div class="items">
                    <div class="c-titleItem">
                        <h2>回収品目例</h2>
                    </div>
                    <div class="c-contentItem">
                        <ul class="c-contentItem__list">
                            <li>
                                <div class="c-contentItem__list__sList">
                                    <ul>
                                        <li><a href="">洗浄機</a></li>
                                        <li><a href="">作業台</a></li>
                                        <li><a href="">業務用冷蔵庫</a></li>
                                        <li><a href="">シンク</a></li>
                                        <li><a href="">ガスレンジ</a></li>
                                        <li><a href="">薬味入れ</a></li>
                                        <li><a href="">寸銅鍋</a></li>
                                        <li><a href="">製氷機</a></li>
                                    </ul>
                                </div>
                            </li>
                            <li>
                                <div class="c-contentItem__list__sList">
                                    <ul>
                                        <li><a href="">フライヤー</a></li>
                                        <li><a href="">電子レンジ</a></li>
                                        <li><a href="">ミキサー</a></li>
                                        <li><a href="">スライサー</a></li>
                                        <li><a href="">ショーケース</a></li>
                                        <li><a href="">コールドテーブル</a></li>
                                        <li><a href="">小物</a></li>
                                        <li><a href="">冷蔵庫</a></li>
                                    </ul>
                                </div>
                            </li>
                            <li>
                                <div class="c-contentItem__list__sList">
                                    <ul>
                                        <li><a href="">自動販売機</a></li>
                                        <li><a href="">券売機 </a></li>
                                        <li><a href="">ショーケース</a></li>
                                        <li><a href="">ラック</a></li>
                                        <li><a href="">レジ</a></li>
                                        <li><a href="">食器</a></li>
                                        <li><a href="">OA機器</a></li>
                                        <li><a href="">パソコン</a></li>
                                    </ul>
                                </div>
                            </li>
                            <li>
                                <div class="c-contentItem__list__sList">
                                    <ul>
                                        <li><a href="">机（デスク）</a></li>
                                        <li><a href="">椅子（チェア）</a></li>
                                        <li><a href="">カウンター</a></li>
                                        <li><a href="">ロッカー</a></li>
                                        <li><a href="">ビジネスフォン</a></li>
                                        <li><a href="">テーブル</a></li>
                                        <li><a href="">テレビ</a></li>
                                        <li><a href="">掃除機</a></li>
                                    </ul>
                                </div>
                            </li>
                            <li>
                                <div class="c-contentItem__list__sList">
                                    <ul>
                                        <li><a href="">ソファ</a></li>
                                        <li><a href="">棚</a></li>
                                        <li><a href="">タンス</a></li>
                                        <li><a href="">生活用品<br>その他不用品など</a></li>
                                    </ul>
                                <div>
                            </li>
                        </ul>
                        <div class="c-contentItem__image">
                            <ul>
                                <li><a href=""><img src="images/img_01.png" alt="img01" width="80" height="80"></a></li>
                                <li><a href=""><img src="images/img_02.png" alt="img02" width="80" height="80"></a></li>
                                <li><a href=""><img src="images/img_03.png" alt="img03" width="80" height="80"></a></li>
                                <li><a href=""><img src="images/img_04.png" alt="img04" width="80" height="80"></a></li>
                                <li><a href=""><img src="images/img_05.png" alt="img05" width="80" height="80"></a></li>
                                <li><a href=""><img src="images/img_06.png" alt="img06" width="80" height="80"></a></li>
                                <li><a href=""><img src="images/img_07.png" alt="img07" width="80" height="80"></a></li>
                            </ul>
                        </div>
                        <div class="c-contentItem__info">
                            <h2>他にもいろいろお取り扱いしております！</h2>
                        </div>
                    </div>
                </div>
                <div class="collection">
                    <div class="c-titleMain">
                        <h2>買取・回収イメージ</h2>
                    </div>
                    <div class="collection_body">
                        <div class="thumbnail">
                            <span>回収前</span>
                        </div>
                        <img src="images/arrow_houjin.png" width="45" height="35" alt="arrow houjin" class="arrow_houjin">
                        <div class="thumbnail">
                            <span>回収前</span>
                        </div>
                    </div>
                </div>
            </div>
            <?=$flow_purchase; ?>
            <br><br>
            <?=$map; ?>
        </div>
        <?php require_once($_SERVER['DOCUMENT_ROOT']."/include/side.php"); ?>
    </div>
<?=$footer; ?>