<?php $id="contact_us"; ?>
<?php require_once($_SERVER['DOCUMENT_ROOT']."/include/init.php"); ?>
<?=$head1; ?>
<?=$header; ?>

    <div class="c-path">
        <ul>
            <li><a href="./">ホーム </a></li>
            <li><a href="#">会社概要</a></li>
        </ul>
    </div>
    <div class="contactUs">
        <div class="l-main">
            <div class="l-conts">
                <div class="c-titleMain">
                    <h2>会社概要</h2>
                </div>
                <div class="c-contact">
                    <fieldset>
                        <table>
                            <tbody>
                                <tr>
                                    <th>会社名</th>
                                    <td>リサイクルマスター英雄</td>
                                </tr>
                                <tr>
                                    <th>代表者</th>
                                    <td>斉藤　誠人</td>
                                </tr>
                                <tr>
                                    <th>古物商許可証</th>
                                    <td>第985号</td>
                                </tr>
                                <tr>
                                    <th>事務所所在地</th>
                                    <td>京都板橋区小豆沢2-21-2<br>
                                        千葉県市川市日の出22-1<br>
                                        千葉県松戸市和名ヶ谷1333<br>
                                        茨城県下妻市五箇530-8
                                    </td>
                                </tr>
                                <tr>
                                    <th>電話番号(フリーダイヤル)</th>
                                    <td>0120-485-888</td>
                                </tr>
                                <tr>
                                    <th>携帯電話</th>
                                    <td>070-2177-7772</td>
                                </tr>
                            </tbody>
                        </table>
                    </fieldset>
                </div>
                <?=$map; ?>
            </div>
            <?php require_once($_SERVER['DOCUMENT_ROOT']."/include/side.php"); ?>
        </div>

    </div>
<?=$footer;  ?>