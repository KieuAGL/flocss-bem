<?php $id="top"; ?>
<?php require_once($_SERVER['DOCUMENT_ROOT']."/include/init.php"); ?>
<?=$head1; ?>
<?=$header; ?>

<link rel="stylesheet" type="text/css" href="/js/slick/slick.css">
<script src="/js/slick/slick.js"></script>

<script>
    $(function(){
        $('.c-slide').slick({
            centerMode: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: true,
            autoplay: true,
            dots: true,
            pauseOnHover: false,
            fade: false,
            speed: 1000,	
            autoplaySpeed: 3000,
            centerPadding: '0',
            variableWidth: true,
            prevArrow: '<button type="button" data-role="none" class="slick-prev" aria-label="Previous" tabindex="0" role="button"></button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next" aria-label="Next" tabindex="0" role="button"></button>',
        });
    })
</script>

<!--<script type="text/javascript">
    var path = window.location.pathname;
    $(function(){
            $('.c-hBox__rNavi ul li a').each(function(){
                if($('.c-hBox__rNavi ul li a').attr('href') == path)
                $('.c-hBox__rNavi ul li a span').addClass('activeNav');
            })
    });

</script>-->

<div class="p-top">
    <div class="l-main">
        <div class="c-slide">
            <div><img src="images/Slide_top.png" width="1000" height="400"></div>
            <div><img src="images/Slide_top.png" width="1000" height="400"></div>
            <div><img src="images/Slide_top.png" width="1000" height="400"></div>
            <div><img src="images/Slide_top.png" width="1000" height="400"></div>
        </div>
        <div class="content">
            <section class="p-top1">
                <div class="c-leftBox">
                    <div class="c-leftBox__title">
                        <h2>個人さま向け</h2>
                    </div>
                    <div class="c-leftBox__content">
                        <div class="thumbnail">
                            <img src="images/photo_01.png" width="111" height="111" alt="img_01">
                            <div class="txt">
                                <h3>粗大 ゴミ・家具・家電・生活雑貨の処分</h3>
                                <span>片付けや引越しなどでいらなくなった 家庭から出る不用品を回収いたします。</span>
                            </div>
                        </div>
                        <div class="thumbnail">
                            <img src="images/photo_02.png" width="111" height="111" alt="img_03">
                            <div class="txt">
                                <h3>ゴミ屋敷整理</h3>
                                <span> 気がついたらいらない物がいっぱいになっていませんか？<br>
                                    何から手をつけていいのか悩んでしまうようなお片づけにも対応いたします。
                                    ワケあり物件もOK です。</span>
                            </div>
                        </div>
                        <div class="thumbnail">
                            <img src="images/photo_03.png" width="111" height="111" alt="img_04">
                            <div class="txt">
                                <h3>遺品整理</h3>
                                <span>お時間がなかなかとれないご家族に代わって大事な思い出の品を大切に処理いたします。</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="c-rightBox">
                    <div class="c-rightBox__title">
                        <h2>法人さま向け</h2>
                    </div>
                    <div class="c-rightBox__content">
                         <div class="thumbnail">
                            <img src="images/photo_04.png" width="111" height="111" alt="img_01">
                            <div class="txt">
                                <h3>オフィス家具・事務用品・<br>OA機器</h3>
                                <span>オフィス家具から事務用品まで丸ごと回収いたします。</span>
                            </div>
                        </div>
                        <div class="thumbnail">
                            <img src="images/photo_05.png" width="111" height="111" alt="img_03">
                            <div class="txt">
                                <h3>作業用工具・業務用厨房機器・店内用品</h3>
                                <span>業務用大型機器から小型工具まで丸ごと回収いたします。</span>
                            </div>
                        </div>
                        <div class="thumbnail">
                            <img src="images/photo_06.png" width="111" height="111" alt="img_04">
                            <div class="txt">
                                <h3>工場・倉庫・ガレージ・<br>看板撤去</h3>
                                <span>工場や倉庫。ガレージ内の不用品を丸ごと回収いたします。</span>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="p-top2 clearfix">
                <div class="c-transBox">
                    <div class="c-transBox__title">
                        <h2>1K程度の小さなお部屋の片付けに</h2>
                    </div>
                    <div class="c-transBox__content">
                        <h4>軽（平）トラック1台積み詰め放題</h4>
                        <span>￥19,000-</span>
                        <ul>
                            <li><a href="">1Fの作業員1名（助手1名5,000円〜</a></li>
                            <li><a href="">2F〜最大25,000円</a></li>
                        </ul>
                        <div class="c-transBox__content__notice">
                            <h5>※家電・鋳物のみなら￥16,000-</h5>
                            <p>※不用品回収作業60分程度を基本料金の最低目安にしております。</p>
                        </div>
                    </div>
                </div>
                <div class="c-transBox02">
                    <div class="c-transBox02__title">
                        <h2>1K〜1DK程度のお部屋の片付けに</h2>
                    </div>
                    <div class="c-transBox02__content">
                        <h4>1トン平トラック1台積み詰め放題</h4>
                        <span>￥29,000-</span>
                        <ul>
                            <li><a href="">1Fの作業員1名<br>（助手1名5,000円〜）</a></li>
                            <li><a href="">2F〜最大35,000円</a></li>
                        </ul>
                        <div class="c-transBox02__content__notice">
                            <p>※不用品回収作業60分程度を基本料金の最低目安にしております。</p>
                        </div>
                    </div>
                </div>
                <div class="c-transBox03">
                    <div class="c-transBox03__title">
                        <h2>1DK〜2DK程度のお部屋の片付けに</h2>
                    </div>
                    <div class="c-transBox03__content">
                        <h4>2トン平トラック1台積み詰め放題</h4>
                        <span>￥40,000-</span>
                        <ul>
                            <li><a href="">1Fの作業員1名<br>（助手1名5,000円〜）</a></li>
                            <li><a href="">2F〜最大50,000円</a></li>
                        </ul>
                        <div class="c-transBox03__content__notice">
                            <p>※不用品回収作業90分程度を基本料金の最低目安にしております。</p>
                        </div>
                    </div>
                </div>
                <div class="c-transBox04">
                    <div class="c-transBox04__title">
                        <h2>2DK〜2LDK程度のお部屋の片付けに</h2>
                    </div>
                    <div class="c-transBox04__content">
                        <h4>2トン箱トラック1台積み詰め放題</h4>
                        <span>￥80,000-</span>
                        <ul>
                            <li><a href="">1Fの作業員2名<br>（助手1名5,000円〜）</a></li>
                            <li><a href="">2F〜最大95,000円</a></li>
                        </ul>
                        <div class="c-transBox04__content__notice">
                            <p>※不用品回収作業120分程度を基本料金の最低目安にしております。</p>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>
 <div class="check">
     <h2>リサイクルマスター英雄はお客様満足度No.1を目指しています</h2>
 </div>
<div class="p-top">
    <div class="l-main">
        <div class="l-conts">
            <p>
                リサイクルマスター英雄では、法人のお客様を対象とした業務用厨房機器・店舗用品・オフィス機器・オフィス用品の買取、無料回収、格安処分を行っております。また、個人のお客様を対象とした家電家具・パソコン・ゲーム機・不用品・ゴミ・遺品の買取・回収・処分等も幅広く行っております。<br>
                引っ越しや店舗移転の際に出る多量の不用品の処分やゴミ屋敷の処分など、不用品でお困りの方の力になりますので、ぜひお気軽にご相談ください。 
            </p>
            <p>
                また、リサイクルマスター英雄では、「どこよりも高く買い取り、どこよりも安く処分できる」をモットーにお客様の負担を少しでも減らせるように意識しています。対応に関しましても、「即日対応・無料見積り・無料相談・テキパキとした対応・動き」を基軸とし、日々の業務に取り組んでいます。 <br>
                価格面・対応面の両方において、不用品買取・回収・処分業界における、ナンバーワンの満足度を目指しています。不用品処分をお考えの方は是非一度リサイクルマスター英雄にご相談下さい！お客様の不用品処分を全力でサポート致します！ 
            </p>
            <section class="p-top3">
                <div class="c-titleMain">
                    <h2>初めての方へ </h2>
                </div>
                <p>
                    リサイクルマスター英雄では、法人様から個人様まで、不用品等の買取・無料回収・格安処分等を行っております。
                    大量の処分品でも当社で分別作業して廃棄致しますのでお時間がない方もお気軽にお申込みください！<br>
                    ※ 新しい家電・ブランド家具等で良い物は高価買取できる場合もございます。 
                </p>
                <div class="reuse">
                    <div class="c-titleReuse">
                        <h2>リサイクルマスター英雄３つの特徴</h2>
                    </div>
                    <div class="c-contentReuse">
                        <div class="c-contentReuse__block">
                            <div class="c-contentReuse__block__image">
                                <img src="images/img_block01.jpg" alt="img_block01" width="222" height="208">
                            </div>
                            <div class="c-contentReuse__block__content">
                                <h3>同時に丸ごと買取・無料回収・格安処分でお得！</h3>
                                <p>リサイクルマスター英雄では、多量処分にも対応致します！不用となった電化製品・家具・家電・オフィス用品・業務用厨房機器・パソコン・ゲーム等の処分でお困りでしたら、お気軽にご連絡下さい！不用品の丸ごと買取・無料回収・格安処分が同時にできますので、お時間のない方でも便利でお得です！また、オフィス・店舗をはじめ、家一軒丸ごとの片付け等も行っており、素早く丁寧をモットーに仕事をしております！ </p>
                                <ul>
                                    <li class="info01">丸ごと買取  </li>
                                    <li class="signal"> + </li>
                                    <li class="info02">無料回収 </li> 
                                    <li class="signal"> + </li>
                                    <li class="info03">格安処分</li>
                                </ul>
                            </div> 
                        </div>
                        <div class="c-contentReuse__block">
                            <div class="c-contentReuse__block__image">
                                <img src="images/img_block02.jpg" alt="img_block02" width="222" height="208">
                            </div>
                            <div class="c-contentReuse__block__content">
                                <h3>即日対応可能でお得！ </h3>
                                <p>リサイクルマスター英雄は、即日対応可能です！お電話一本で、不用品を回収致します。まずはお見積りを致しますので、お気軽にお問い合わせ下さい。</p>
                                <ul>
                                    <li class="info01">即日対応可能 </li>
                                </ul>
                            </div> 
                        </div>
                         <div class="c-contentReuse__block">
                            <div class="c-contentReuse__block__image">
                                <img src="images/img_block03.jpg" alt="img_block02" width="222" height="208">
                            </div>
                            <div class="c-contentReuse__block__content">
                                <h3>どこよりも高く買い取り、安く処分でお得！ </h3>
                                <p>リサイクルマスター英雄は不用品買取・処分・ゴミ回収の業界No.1を目指しております。無料お見積もり後、万一他社様と比べて1円でも高かった場合は、再度お見積もりをし、お客様が納得できる金額を再度ご提示させて頂きます！</p>
                                <ul>
                                    <li class="info01">高価買取 </li>
                                    <li class="signal"> + </li>
                                    <li class="info02">格安処分 </li> 
                                </ul>
                            </div> 
                        </div>
                    </div>
                </div>
            </<section>
                 <div class="clearfix"></div>
            <section class="p-top4">
               <?=$shopping_item; ?>
            </section>
            <section class="p-top5">   
                <?=$map; ?>
            </section>
            </div>
            <!-- Side Bar -->
            <?php require_once($_SERVER['DOCUMENT_ROOT']."/include/side.php"); ?>
        <!-- End Side Bar -->
        </div>
    </div>
</div>
</div>


<!-- Footer -->
    <?=$footer; ?>
<!-- End Footer -->