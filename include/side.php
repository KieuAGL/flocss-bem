<?php ?>

<div class="l-sideBar">
    <div class="sidebar_banner">
        <p>
            <a href="#">
                <img src="images/sidebar_banner.png" width="251" height="231" alt="sidebar_banner">
            </a>
        </p>
    </div>
    <div class="Box1">
        <div class="c-title">
            <span>法人のお客様 </span>  
        </div>
        <ul class="c-sNavi">
            <li><a href="houjin-gyoumuyou.php">業務用厨房機器買取・無料回収・格安処分</a></li>
            <li><a href="houjin-tenpo.php">店舗用品買取・無料回収・格安処分</a></li>
            <li><a href="houjin-office.php">オフィス機器買取・無料回収・格安処分 </a></li>
        </ul>                  
    </div>
    <div class="Box2">
        <div class="c-title">
            <span>個人のお客様 </span>  
        </div>
        <ul class="c-sNavi">
            <li><a href="kojin-kagukaden.php">家具家電買取・無料回収 </a></li>
            <li><a href="kojin-pcgame.php">パソコン・ゲーム機・<br>
                            小型家電無料回収 </a></li>
            <li><a href="kojin-gomiihin.php">不用品回収・ゴミ処分・遺品整理 </a></li>
        </ul>                  
    </div>
    <div class="Box3">
        <ul class="c-sNavi">
            <li><a href="#">買取・回収の流れ </a></li>
            <li><a href="#">価格表 </a></li>
            <li><a href="#">よくある質問 </a></li>
            <li><a href="#">会社概要 </a></li>
        </ul>                  
    </div>
</div>

<!--<script>
    $('document').ready(function(){
        $('.c-sNavi li').click(function(){
            $(this).siblings().removeClass('active');
            $(this).addClass('active');
        });
    });
</script>-->