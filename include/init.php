<?php

//==============================================
 // Title
 //==============================================   
$title = "";
if($id=="top"){
    $title = "Index";
}elseif($id=="flow_of_purchase"){    $title="Purchase";
}elseif($id=="price_list"){          $title="Price List";
}elseif($id=="FAQ"){                 $title="FAQ";
}elseif($id=="contact_us"){          $title="Contact Us";
}elseif($id=="houjin_gyoumuyou"){    $title="Houjin Gyoumuyou";
}elseif($id=="houjin_tenpo"){        $title="Houjin Tenpo";
}elseif($id=="houjin_office"){       $title="Houjin Office";
}elseif($id=="houjin"){              $title="Houjin";
}elseif($id=="kojin_kagukaden"){     $title="Kojin Kagukaden";
}elseif($id=="kojin_PCgame"){        $title=" Kojin PCgame";
}elseif($id=="kojin_gomiihin"){      $title="Kojin Gomiihin";
}elseif($id=="kojin"){               $title="Kojin";
}
 //==============================================
 // Header
 //==============================================   
$head1 = <<< EOM
    <!DOCTYPE html>
    <html lang="jp">
    <head>
        <title>{$title}</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="css/style.css" rel="stylesheet">
        <script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
        <script type="text/js" src="/js/myscript.js"></script>
    </head>
     <body>
        <div class="container">
EOM;

$header = <<< EOM
    <div class="l-header">
        <div class="c-hBox clearfix">
            <h1 class="c-hBox__logo">
                <a href="/">
                    <div>
                        <img src="images/logo.png" width="104" height="60" alt="logo"/>
                    </div>
                </a>
            </h1>
            <div class="c-hBox__rNavi">
                <ul class="clearfix">
                    <li><a href="/">ホーム<span>Home</span></a></li>
                    <li><a href="purchase.php">買取・回収の流れ<span>Flow of Purchase & Collect</span></a></li>
                    <li><a href="pricelist.php">価格表<span>Price List</span></a></li>
                    <li><a href="faq.php">よくある質問<span>FAQ</span></a></li>
                    <li><a href="#">会社概要<span>About Us</span></a></li>
                </ul>
            </div>
        </div>
    </div>
EOM;

//===============================================
// Footer
//===============================================
$footer = <<< EOM
        <div class="c-pageTop clearfix">
            <button onclick="document.body.scrollTop = 0;">
                <a href="#pageTop">
                <img src="images/pageTop.png" alt="page_Top" width="50" height="20"/>
            </a>
            </button>
        </div>
        <footer class="l-footer clearfix">
            <div class="c-footer">
                <ul class="c-footer__Title">
                    <li><a href="#">サイトマップ</a></li>
                    <li><a href="#">ホーム </a></li>
                    <li><a href="#">買取・回収の流れ </a></li>
                    <li><a href="#">価格表 　</a></li>
                    <li><a href="#">よくある質問 </a></li>
                    <li><a href="#">会社概要</a></li>
                </ul>
                <ul class="c-fNaviUl clearfix">
                    <li class="c-fNaviUl__g1">
                        <ul>
                            <li><a href="#">法人のお客様 </a></li>
                            <li><a href="#">業務用厨房機器買取・無料回収・格安処分 </a></li>
                            <li><a href="#">店舗用品買取・無料回収・格安処分 </a></li>
                            <li><a href="#">オフィス機器買取 ・無料回収・格安処分 </a></li>
                        </ul>
                    </li>
                    <li class="c-fNaviUl__g2">
                        <ul>
                            <li><a href="#">個人のお客様 </a></li>
                            <li><a href="#">家具家電買取・無料回収・格安処分 </a></li>
                            <li><a href="#">パソコン・ゲーム機器・小型家電無料回収 </a></li>
                            <li><a href="#">不用品回収・ゴミ処分・遺品整理</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </footer>
        <div class="coppy-right clearfix">
            <h3>CopyRight © リサイクルマスター英雄 All Right Reserved.</h3>
        </div>
    </body>
</html>
EOM;

//===============================================
// Shopping Item
//===============================================
$shopping_item = <<< EOM
    <div class="c-titleMain">
        <h2>買取回収品目</h2>
    </div>
    <p>
        業務用厨房機器・店舗用品・オフィス機器・オフィス用品・PC・ゲーム・家電家具等。リサイクルマスター英雄では法人のお客様から個人のお客様まで幅広く様々な品を承ります。回収品目に関しましてはお気軽にご相談下さい。 
    </p>
    <div class="items">
        <div class="c-titleItem">
            <h2>回収品目例</h2>
        </div>
        <div class="c-contentItem">
            <ul class="c-contentItem__list">
                <li>
                    <div class="c-contentItem__list__sList">
                        <ul>
                            <li><a href="">洗浄機</a></li>
                            <li><a href="">作業台</a></li>
                            <li><a href="">業務用冷蔵庫</a></li>
                            <li><a href="">シンク</a></li>
                            <li><a href="">ガスレンジ</a></li>
                            <li><a href="">薬味入れ</a></li>
                            <li><a href="">寸銅鍋</a></li>
                            <li><a href="">製氷機</a></li>
                        </ul>
                    </div>
                </li>
                <li>
                    <div class="c-contentItem__list__sList">
                        <ul>
                            <li><a href="">フライヤー</a></li>
                            <li><a href="">電子レンジ</a></li>
                            <li><a href="">ミキサー</a></li>
                            <li><a href="">スライサー</a></li>
                            <li><a href="">ショーケース</a></li>
                            <li><a href="">コールドテーブル</a></li>
                            <li><a href="">小物</a></li>
                            <li><a href="">冷蔵庫</a></li>
                        </ul>
                    </div>
                </li>
                <li>
                    <div class="c-contentItem__list__sList">
                        <ul>
                            <li><a href="">自動販売機</a></li>
                            <li><a href="">券売機 </a></li>
                            <li><a href="">ショーケース</a></li>
                            <li><a href="">ラック</a></li>
                            <li><a href="">レジ</a></li>
                            <li><a href="">食器</a></li>
                            <li><a href="">OA機器</a></li>
                            <li><a href="">パソコン</a></li>
                        </ul>
                    </div>
                </li>
                <li>
                    <div class="c-contentItem__list__sList">
                        <ul>
                            <li><a href="">机（デスク）</a></li>
                            <li><a href="">椅子（チェア）</a></li>
                            <li><a href="">カウンター</a></li>
                            <li><a href="">ロッカー</a></li>
                            <li><a href="">ビジネスフォン</a></li>
                            <li><a href="">テーブル</a></li>
                            <li><a href="">テレビ</a></li>
                            <li><a href="">掃除機</a></li>
                        </ul>
                    </div>
                </li>
                <li>
                    <div class="c-contentItem__list__sList">
                        <ul>
                            <li><a href="">ソファ</a></li>
                            <li><a href="">棚</a></li>
                            <li><a href="">タンス</a></li>
                            <li><a href="">生活用品<br>その他不用品など</a></li>
                        </ul>
                    <div>
                </li>
            </ul>
            <div class="c-contentItem__image">
                <ul>
                    <li><a href=""><img src="images/img_01.png" alt="img01" width="80" height="80"></a></li>
                    <li><a href=""><img src="images/img_02.png" alt="img02" width="80" height="80"></a></li>
                    <li><a href=""><img src="images/img_03.png" alt="img03" width="80" height="80"></a></li>
                    <li><a href=""><img src="images/img_04.png" alt="img04" width="80" height="80"></a></li>
                    <li><a href=""><img src="images/img_05.png" alt="img05" width="80" height="80"></a></li>
                    <li><a href=""><img src="images/img_06.png" alt="img06" width="80" height="80"></a></li>
                    <li><a href=""><img src="images/img_07.png" alt="img07" width="80" height="80"></a></li>
                </ul>
            </div>
            <div class="c-contentItem__info">
                <h2>他にもいろいろお取り扱いしております！</h2>
            </div>
        </div>
    </div>
EOM;

//===============================================
// Map
//===============================================
$map = <<< EOM
<div class="c-titleMain">
    <h2>対応地域一覧 </h2>
</div>
<div class="c-entry-photo">
    <img src="images/map.png" width="452" height="380" alt="map">
</div>
<div class="c-entry-body">
    <div class="c-entry-body__01">
        <img src="images/icon_entry01.png" width="130" height="24" alt="icon_entry_01">
        <p>
            23区・中央区・港区・新宿区・文京区・台東区・千代田区・墨田区・江東区・品川区・目黒区・大田区・世田谷区・渋谷区・中野区・杉並区・豊島区・北区・荒川区・板橋区・練馬区・足立区・葛飾区・江戸川区・八王子市・田無市・立川市・武蔵野市・三鷹市・府中市・昭島市・調布市・町田市・小金井市・小平市・日野市・東村山市・国分寺市・国立市・西東京市・福生市・狛江市・東大和市・清瀬市・東久留米市・武蔵村山市・多摩市・稲城市・あきる野市・羽村市・青梅市 その他・・・ 
        </p>
    </div>
    <div class="c-entry-body__02">
            <img src="images/icon_entry02.png" width="130" height="24" alt="icon_entry_02">
        <p>
            さいたま市/西区・岩槻区・緑区・見沼区・浦和区・南区・北区・大宮区・中央区・桜区・川口市・蕨市・戸田市・上尾市・伊奈町・川越市・所沢市・狭山市・入間市・富士見市・新座市・志木市・朝霞市・和光市・ふじみ野市・入間郡三芳町・越谷市・草加市・春日部市・三郷市・八潮市・吉川市・松伏町・南埼玉郡宮代町・白岡市・久喜市・北葛飾郡杉戸町・・・ 
        </p>
    </div>
    <div class="c-entry-body__03">
        <img src="images/icon_entry03.png" width="130" height="24" alt="icon_entry_03">
        <p>
            千葉市/中央区・若葉区・緑区・美浜区・稲毛区・花見川区・印旛郡栄町印西市・浦安市・我孫子市・鎌ヶ谷市・四街道市・市原市・市川市・習志野市・松戸市・船橋市・柏市・佐倉市・白井市・八千代市・野田市・流山市・成田市・富里市・八街市・・・茨城県守谷市・利根町・取手市 
        </p>
    </div>
    <p>その他神奈川県・茨城県の一部地域も対応しておりますので、ご相談ください。 </p>
</div>
EOM;


//===============================================
// Flow Purchase
//===============================================
$flow_purchase = <<< EOM
<div class="c-flow">
    <div class="c-titleMain">
        <h2>買取・回収の流れ</h2>
    </div>
    <div class="c-flow__body">
        <img src="images/flow.png" width="645" height="135" alt="flow_purchase">
        <section class="c-flow__body__feature01">
            <div class="feature01-content">
                <span class="redS">無料お見積り</span>
                <p>まずはお見積りをさせていただきます。相談・お見積りは無料！即日・夜間でも対応が可能です！</p>
                <div class="contact">
                    <h4>お気軽にお問い合わせ下さい</h4>
                    <p>
                        <img src="images/contact_feature.png" width="47" height="25" alt="contact_feature">
                        <span class="tell">0120-485-888</span> 携帯・PHSからもOK!
                    </p>
                    <p>お電話がつながりにくい場合  070-2177-7772 <span class="redS2">24時間対応</span></p>
                </div>
            </div>
            <div class="feature01-image">
                <img src="images/photo_feature01.png" width="220" height="180" alt="image_feature_01">
            </div>
        </section>
        <img src="images/arrow_feature.png" width="36" height="45" alt="" class="arrowF">
        <section class="c-flow__body__feature02">
            <div class="feature02-content">
                <span class="redS">買取・回収のご契約</span>
                <p>ご契約後、御希望の回収日時にお伺い致します。</p>
            </div>
            <div class="feature02-image">
                <img src="images/photo_feature02.png" width="222" height="160" alt="image_feature_02">
            </div>
        </section>
            <img src="images/arrow_feature.png" width="36" height="45" alt="" class="arrowF1">
        <section class="c-flow__body__feature03">
            <div class="feature03-content">
                <span class="redS">回収作業</span>
                <p>回収作業終了後、ご契約いただいた金額でご精算させていただきます。</p>
            </div>
            <div class="feature03-image">
                    <img src="images/photo_feature03.png" width="222" height="160" alt="image_feature_03">
            </div>
        </section>
    </div>
</div>
EOM;
?>


