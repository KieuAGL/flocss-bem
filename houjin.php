<?php $id="houjin_office"; ?>
<?php require_once($_SERVER['DOCUMENT_ROOT']."/include/init.php"); ?>
<?=$head1; ?>
<?=$header; ?>

    <div class="c-path">
        <ul>
            <li><a href="./">ホーム </a></li>
            <li><a href="#">法人のお客様 </a></li>
        </ul>
        <div class="checkHoujin">
            <h2>法人のお客様</h2>
        </div>
    </div>
    <div class="coporation">
        <ul>
            <li><a><img src="images/houjin_photo01.png" alt="houjin_photo01" width="320" height="180"> </a></li>
            <li><a><img src="images/houjin_photo02.png" alt="houjin_photo02" width="320" height="180"> </a></li>
            <li><a><img src="images/houjin_photo03.png" alt="houjin_photo03" width="320" height="180"> </a></li>
        </ul>
    </div>
    <div class="houjin">
        <div class="l-main">
            <div class="l-conts"></div>
            <?php require_once($_SERVER['DOCUMENT_ROOT']."/include/side.php"); ?>
        </div>
    </div>
<?=$footer; ?>